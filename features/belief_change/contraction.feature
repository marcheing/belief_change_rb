Feature: Belief Change Contraction ~ Consistent Contraction

  Scenario: Belief Change Contraction using a simple formula set
    Given I have a formula set
    And I add a formula that reads a
    And I add a formula that reads b
    When I consistently contract by the formula that reads a
    And I ask if the formula set entails the formula a
    Then I should get false
    And I ask if the formula set entails the formula b
    Then I should get true
  
  Scenario: Belief Change Contraction using a more complex formula set
    Given I have a formula set
    And I add a formula that reads a
    And I add a formula that reads b
    And I add a formula that reads ¬a v c
    And I add a formula that reads ¬b v c
    When I consistently contract by the formula that reads c
    And I ask if the formula set entails the formula c
    Then I should get false
  
  Scenario: Belief Change Contraction using a more complex formula set
    Given I have a formula set
    And I add a formula that reads ¬b v a
    And I add a formula that reads b
    And I add a formula that reads ¬b v a
    And I add a formula that reads ¬c v ¬d v b
    And I add a formula that reads ¬e v d
    And I add a formula that reads c
    And I add a formula that reads e
    When I consistently contract by the formula that reads a
    And I ask if the formula set entails the formula a
    Then I should get false
