Feature: Belief Change Contraction ~ Exceptional cases

  Scenario: Belief change contraction creating formulas with double negation
    Given I have a formula set
    And I add a formula that reads e v g v ¬b
    And I add a formula that reads c v ¬h v a
    And I add a formula that reads a v ¬b v ¬j
    And I add a formula that reads ¬h v ¬e v ¬b
    And I add a formula that reads i v ¬e v ¬g
    And I add a formula that reads ¬j v h v g
    And I add a formula that reads b v ¬g v e
    And I add a formula that reads ¬d v c v ¬a
    And I add a formula that reads ¬b v ¬g v f
    And I add a formula that reads c v ¬i v ¬e
    And I add a formula that reads ¬c v e v ¬a
    And I add a formula that reads i v ¬c v g
    And I add a formula that reads g v i v ¬f
    And I add a formula that reads f v ¬c v a
    And I add a formula that reads ¬j v ¬i v ¬h
    And I add a formula that reads f v b v g
    And I add a formula that reads ¬c v ¬f v i
    And I add a formula that reads ¬f v a v ¬j
    And I add a formula that reads ¬c v a v i
    And I add a formula that reads c v b v ¬j
    And I add a formula that reads ¬f v ¬d v ¬j
    And I add a formula that reads g v e v ¬f
    And I add a formula that reads j v g v ¬d
    And I add a formula that reads a v ¬h v e
    And I add a formula that reads f v b v ¬j
    When I consistently contract by the formula that reads g v d v ¬j
    And I ask if the formula set entails the formula g v d v ¬j
    Then I should get false
