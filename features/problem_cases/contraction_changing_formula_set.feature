Feature: Any Contraction altering the belief set

  Scenario: Created belief set having all atoms
    Given I have a formula set
    And I add a formula that reads e v g v ¬b
    And I add a formula that reads c v ¬h v a
    Then the formula set should have the atoms a b c e g h
    And every formula from the set should have the atoms a b c e g h
    And every formula from the set should have the same atoms map
  
  Scenario: Kernel contraction changing the set
    Given I have a formula set
    And I add a formula that reads e v g v ¬b
    And I add a formula that reads c v ¬h v a
    And I add a formula that reads a v ¬b v ¬j
    And i create a copy of the set
    When I call kernel contraction by the formula that reads a v ¬b v ¬j
    Then the copy and the set should be identical
  
  Scenario: Maxichoice partial meet contraction changing the set
    Given I have a formula set
    And I add a formula that reads e v g v ¬b
    And I add a formula that reads c v ¬h v a
    And I add a formula that reads a v ¬b v ¬j
    And i create a copy of the set
    When I call maxichoice partial meet contraction by the formula that reads a v ¬b v ¬j
    Then the copy and the set should be identical
  
  Scenario: Partial meet contraction changing the set
    Given I have a formula set
    And I add a formula that reads e v g v ¬b
    And I add a formula that reads c v ¬h v a
    And I add a formula that reads a v ¬b v ¬j
    And i create a copy of the set
    When I call partial meet contraction by the formula that reads a v ¬b v ¬j
    Then the copy and the set should be identical
  
  Scenario: Belief change contraction changing the set
    Given I have a formula set
    And I add a formula that reads e v g v ¬b
    And I add a formula that reads c v ¬h v a
    And I add a formula that reads a v ¬b v ¬j
    And i create a copy of the set
    When I call belief change contraction by the formula that reads a v ¬b v ¬j
    Then the copy and the set should be identical
