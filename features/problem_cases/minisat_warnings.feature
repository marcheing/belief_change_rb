Feature: Minisat Reasoner returning warnings

  Scenario: Formula set not using all variables
    Given I have a formula set
    And I add a formula that reads j v ¬c v ¬k
    And I add a formula that reads ¬n v ¬a v ¬e
    And I add a formula that reads ¬l v a v f
    And I add a formula that reads a v d v l
    And I add a formula that reads k v ¬c v e
    When I consistently contract by the formula that reads j v ¬c v k
    And I ask if the formula set entails the formula j v ¬c v k
    Then I should get false
