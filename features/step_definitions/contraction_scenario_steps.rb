require 'factory_girl'

Given(/^I have a contraction scenario$/) do
  FactoryGirl.find_definitions
  @contraction_scenario = FactoryGirl.build(:contraction_scenario)
end

When(/^I ask if the generated formula set entails the generated formula$/) do
  @result = @contraction_scenario.formula_set.entails? @contraction_scenario.formula
end
