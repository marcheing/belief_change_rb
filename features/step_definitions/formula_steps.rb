Given(/^I input a formula that reads (.+)$/) do |input|
  @formula = Formula.new input
end

When(/^I ask if the formula is satisfiable$/) do
  @result = @formula.satisfiable?
end

When(/^I ask if the formula is a tautology$/) do
  @result = @formula.tautology?
end

Then(/^I should get true$/) do
  expect(@result).to be true
end

Then(/^I should get false$/) do
  expect(@result).to be false
end
