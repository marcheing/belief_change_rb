Given(/^I have a formula set$/) do
  @formula_set = FormulaSet.new
end

Given(/^I add a formula that reads (.+)$/) do |input|
  @formula_set.add(Formula.new(input))
end

Given(/^I have a formula set that will be used for contraction$/) do
  @formula_set_for_contraction = FormulaSet.new
end

Given(/^i create a copy of the set$/) do
  @copy = @formula_set.clone
end

When(/^I ask if the formula set is satisfiable$/) do
  @result = @formula_set.satisfiable?
end

When(/^I ask if the formula set entails the formula (.+)$/) do |input|
  @result = @formula_set.entails? Formula.new input
end

When(/^I ask if the formula set completely entails the set (.+)$/) do |input|
  set = FormulaSet.new(input.split(',').map { |f| Formula.new f })
  @result = @formula_set.entails_all? set
end

When(/^I ask if the formula set partially entails the set (.+)$/) do |input|
  set = FormulaSet.new(input.split(',').map { |f| Formula.new f })
  @result = @formula_set.entails_any? set
end

When(/^I kernel contract by the formula that reads (.+)$/) do |input|
  @formula_set = @formula_set.smallest_incision_kernel_contraction(Formula.new input)
end

When(/^I call kernel contraction by the formula that reads (.+)$/) do |input|
  @formula_set.smallest_incision_kernel_contraction(Formula.new input)
end

When(/^I call maxichoice partial meet contraction by the formula that reads (.+)$/) do |input|
  @formula_set.maxichoice_partial_meet_contraction(Formula.new input)
end

When(/^I call partial meet contraction by the formula that reads (.+)$/) do |input|
  @formula_set.partial_meet_contraction(Formula.new input)
end

When(/^I call belief change contraction by the formula that reads (.+)$/) do |input|
  @formula_set.belief_change_contraction(Formula.new input)
end

When(/^I partial meet contract by the formula that reads (.+)$/) do |input|
  @formula_set = @formula_set.maxichoice_partial_meet_contraction(Formula.new input)
end

When(/^I partial meet contract using the original algorithm by the formula that reads (.+)$/) do |input|
  @formula_set = @formula_set.maxichoice_partial_meet_contraction(Formula.new input)
end

When(/^I black box partial meet contract by the formula that reads (.+)$/) do |input|
  @formula_set = @formula_set.full_meet_contraction(Formula.new input)
end

When(/^I consistently contract by the formula that reads (.+)$/) do |input|
  @formula_set = FormulaSet.new [@formula_set.belief_change_contraction(Formula.new input)]
end

When(/^I add to the contraction set a formula that reads (.+)$/) do |input|
  @formula_set_for_contraction.add(Formula.new input)
end

When(/^I apply the kernel package contraction$/) do
  @formula_set = @formula_set.package_kernel_contraction(@formula_set_for_contraction)
end

Then(/^the formula set should have the atoms (.+)$/) do |atoms|
  atoms.split(' ').each { |atom| expect(@formula_set.atoms).to include atom }
end

Then(/^every formula from the set should have the atoms (.+)$/) do |atoms|
  atoms.split(' ').each do |atom|
    @formula_set.each do |formula|
      expect(formula.atoms_map.keys).to include atom
    end
  end
end

Then(/^the copy and the set should be identical$/) do
  expect(@copy).to eq @formula_set
  expect(@copy.atoms).to eq @formula_set.atoms
  expect(@formula_set.to_a).to match_array @copy.to_a
  @copy.each { |f| @formula_set.each { |g| expect(f.atoms_map).to eq(g.atoms_map) } }
end

Then(/^every formula from the set should have the same atoms map$/) do
  map = @formula_set.first.atoms_map
  @formula_set.each do |formula|
    expect(formula.atoms_map).to eq map
  end
end
