Feature: Correctly identifying tautologies

  Scenario: A simple tautology
    Given I input a formula that reads ⊤
    When I ask if the formula is a tautology 
    Then I should get true

  Scenario: A complex tautology
    Given I input a formula that reads a v b v ¬a v ¬b 
    When I ask if the formula is a tautology 
    Then I should get true

  Scenario: No tautology
    Given I input a formula that reads a v b ^ c v ¬c
    When I ask if the formula is a tautology 
    Then I should get false
