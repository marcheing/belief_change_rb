Feature: Satisfiability of formulas

  Scenario: A satisfiable formula
    Given I input a formula that reads a v b ^ c v ¬c
    When I ask if the formula is satisfiable
    Then I should get true

  Scenario: A formula with only the tautology symbol
    Given I input a formula that reads ⊤
    When I ask if the formula is satisfiable
    Then I should get true

  Scenario: A formula with only the contradiction symbol
    Given I input a formula that reads ⊥
    When I ask if the formula is satisfiable
    Then I should get false
