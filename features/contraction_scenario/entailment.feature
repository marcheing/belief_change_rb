Feature: Entailment of formulas in a contraction scenario

  Scenario: Any contraction scenario
    Given I have a contraction scenario
    When I ask if the generated formula set entails the generated formula
    Then I should get true

