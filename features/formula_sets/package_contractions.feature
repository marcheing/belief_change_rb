Feature: Package contractions

  Scenario: Kernel package contraction with simple formula sets
    Given I have a formula set
    And I add a formula that reads ¬a v b
    And I add a formula that reads ¬c v d
    And I add a formula that reads a
    And I add a formula that reads c
    And I add a formula that reads f
    And I have a formula set that will be used for contraction
    And I add to the contraction set a formula that reads b
    And I add to the contraction set a formula that reads d
    And I add to the contraction set a formula that reads g
    # TODO: Finish this scenario
    #When I apply the kernel package contraction
    #And I ask if the formula set entails the formula b
    #Then I should get false
    #When I ask if the formula set entails the formula d
    #Then I should get false
    #When I ask if the formula set entails the formula f
    #Then I should get true
