Feature: Belief Contraction

  Scenario: Kernel Contraction using a simple formula set
    Given I have a formula set
    And I add a formula that reads a
    And I add a formula that reads b
    When I kernel contract by the formula that reads a
    And I ask if the formula set entails the formula a
    Then I should get false
    And I ask if the formula set entails the formula b
    Then I should get true
  
  Scenario: Kernel Contraction using a more complex formula set
    Given I have a formula set
    And I add a formula that reads a
    And I add a formula that reads b
    And I add a formula that reads ¬a v c
    And I add a formula that reads ¬b v c
    When I kernel contract by the formula that reads c
    And I ask if the formula set entails the formula c
    Then I should get false
  
  Scenario: Kernel Contraction using a more complex formula set
    Given I have a formula set
    And I add a formula that reads ¬b v a
    And I add a formula that reads b
    And I add a formula that reads ¬b v a
    And I add a formula that reads ¬c v ¬d v b
    And I add a formula that reads ¬e v d
    And I add a formula that reads c
    And I add a formula that reads e
    When I kernel contract by the formula that reads a
    And I ask if the formula set entails the formula c
    Then I should get false
    When I ask if the formula set entails the formula b
    Then I should get false
    When I ask if the formula set entails the formula a
    Then I should get false

  Scenario: Partial Meet Contraction using a simple formula set
    Given I have a formula set
    And I add a formula that reads ¬a v b
    And I add a formula that reads a
    And I add a formula that reads c
    When I partial meet contract by the formula that reads b
    And I ask if the formula set entails the formula b
    Then I should get false
    When I ask if the formula set entails the formula c
    Then I should get true

  Scenario: Partial Meet Contraction using a complex formula set
    Given I have a formula set
    And I add a formula that reads ¬b v a
    And I add a formula that reads b
    And I add a formula that reads ¬c v ¬d v b
    And I add a formula that reads ¬e v d
    And I add a formula that reads c
    And I add a formula that reads e
    When I partial meet contract by the formula that reads a
    And I ask if the formula set entails the formula a
    Then I should get false
    When I ask if the formula set entails the formula c
    Then I should get true
  
  
  Scenario: Black Box Partial Meet Contraction using a simple formula set
    Given I have a formula set
    And I add a formula that reads ¬a v b
    And I add a formula that reads a
    And I add a formula that reads c
    When I black box partial meet contract by the formula that reads b
    And I ask if the formula set entails the formula b
    Then I should get false
    And I ask if the formula set entails the formula c
    Then I should get true
  
  
  Scenario: Black Box Partial Meet Contraction using a complex formula set
    Given I have a formula set
    And I add a formula that reads ¬a v b
    And I add a formula that reads ¬c v d
    And I add a formula that reads ¬f v a v ¬x
    And I add a formula that reads f
    And I add a formula that reads ¬e v x v ¬f
    And I add a formula that reads e
    When I black box partial meet contract by the formula that reads e
    And I ask if the formula set entails the formula e
    Then I should get false
    And I ask if the formula set entails the formula x
    Then I should get false
    And I ask if the formula set entails the formula b
    Then I should get false
    And I ask if the formula set entails the formula a
    Then I should get false
    And I ask if the formula set entails the formula f
    Then I should get true

  Scenario: General Partial Meet Contraction using a simple formula set
    Given I have a formula set
    And I add a formula that reads ¬a v b
    And I add a formula that reads a
    And I add a formula that reads c
    When I partial meet contract using the original algorithm by the formula that reads b
    And I ask if the formula set entails the formula b
    Then I should get false
    When I ask if the formula set entails the formula c
    Then I should get true

  Scenario: General Partial Meet Contraction using a complex formula set
    Given I have a formula set
    And I add a formula that reads ¬b v a
    And I add a formula that reads b
    And I add a formula that reads ¬c v ¬d v b
    And I add a formula that reads ¬e v d
    And I add a formula that reads c
    And I add a formula that reads e
    When I partial meet contract using the original algorithm by the formula that reads a
    And I ask if the formula set entails the formula a
    Then I should get false
    When I ask if the formula set entails the formula c
    Then I should get true
  
 
