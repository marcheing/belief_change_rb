Feature: Entailment of formulas

  Scenario: Getting the entailments of a simple formula set
    Given I have a formula set
    And I add a formula that reads ¬a v b
    And I add a formula that reads a
    When I ask if the formula set entails the formula a
    Then I should get true
    When I ask if the formula set entails the formula b
    Then I should get true
    When I ask if the formula set entails the formula ¬b
    Then I should get false
    When I ask if the formula set entails the formula c
    Then I should get false
  
  Scenario: Getting the entailments of a complex formula set
    Given I have a formula set
    And I add a formula that reads ¬a v b
    And I add a formula that reads ¬c v d
    And I add a formula that reads ¬b v ¬d v e
    And I add a formula that reads a
    And I add a formula that reads c
    When I ask if the formula set entails the formula e
    Then I should get true
    When I ask if the formula set entails the formula d
    Then I should get true
    When I ask if the formula set entails the formula ¬d v e
    Then I should get true
    When I ask if the formula set entails the formula ¬e
    Then I should get false
  
  Scenario: Getting the set entailments of a simple formula set
    Given I have a formula set
    And I add a formula that reads ¬a v b
    And I add a formula that reads a
    When I ask if the formula set completely entails the set a
    Then I should get true
    When I ask if the formula set completely entails the set a, b
    Then I should get true
    When I ask if the formula set completely entails the set a, c
    Then I should get false
    When I ask if the formula set completely entails the set ¬a, a
    Then I should get false
    When I ask if the formula set partially entails the set a, c
    Then I should get true
    When I ask if the formula set partially entails the set ¬a, a
    Then I should get true
  
  Scenario: Getting the set entailments of a complex formula set
    Given I have a formula set
    And I add a formula that reads ¬a v b
    And I add a formula that reads ¬c v d
    And I add a formula that reads ¬b v ¬d v e
    And I add a formula that reads a
    And I add a formula that reads c
    When I ask if the formula set completely entails the set e 
    Then I should get true
    When I ask if the formula set completely entails the set c, ¬d
    Then I should get false
    When I ask if the formula set completely entails the set b, e, d
    Then I should get true
    When I ask if the formula set partially entails the set c, ¬d
    Then I should get true
  
