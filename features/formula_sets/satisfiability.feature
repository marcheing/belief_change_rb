Feature: Satisfiability of sets of formulas

  Scenario: A satisfiable formula
    Given I have a formula set
    And I add a formula that reads a v b ^ c v ¬c
    When I ask if the formula set is satisfiable
    Then I should get true
  
  Scenario: An unsatisfiable formula
    Given I have a formula set
    And I add a formula that reads a v b ^ c v ¬c ^ ¬a ^ ¬b
    When I ask if the formula set is satisfiable
    Then I should get false
  
  Scenario: A satisfiable formula set
    Given I have a formula set
    And I add a formula that reads a v b ^ c v ¬c
    And I add a formula that reads ¬a v d v h ^ f
    And I add a formula that reads ¬f v h v ¬a ^ b
    When I ask if the formula set is satisfiable
    Then I should get true
  
  Scenario: An unsatisfiable formula set
    Given I have a formula set
    And I add a formula that reads a v b ^ c v ¬c
    And I add a formula that reads ¬a ^ h v f
    And I add a formula that reads ¬b ^ ¬f v h
    When I ask if the formula set is satisfiable
    Then I should get false
  
