require File.expand_path('../../config/environment', __FILE__)
require 'simplecov'
require 'factory_girl'

SimpleCov.start
FactoryGirl.find_definitions

RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
  config.mock_with :mocha
end
