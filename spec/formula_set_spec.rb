RSpec.describe FormulaSet do
  describe 'initialize' do
    context 'with an array of formulas' do
      subject { build(:formula_set) }

      it 'is expected to create a set of formulas' do
        expect(subject).to be_a described_class
        expect(subject.size).to be > 0
      end
    end

    context 'without any formula' do
      subject { build(:formula_set, :empty) }

      it 'is expected to create an empty set' do
        expect(subject).to be_a described_class
        expect(subject.size).to eq 0
        expect(subject).to be_empty
      end
    end

    context 'with multiple formula' do
      context 'with the same atoms' do
        let(:formula_1) { build(:formula, clauses: 'a v b') }
        let(:formula_2) { build(:formula, clauses: '¬a v ¬b') }

        it 'attributes the same atoms hash to both formulas' do
          described_class.new [formula_1, formula_2]
          expect(formula_1.atoms_map.keys).to eq(formula_2.atoms_map.keys)
        end
      end

      context 'with different atoms' do
        let(:formula_1) { build(:formula, clauses: 'a v b') }
        let(:formula_2) { build(:formula, clauses: '¬a v ¬b v c') }
        let(:formula_3) { build(:formula, clauses: 'd v e v f') }

        it "sets the formulas' atoms_map to one that's the same between every formula in the set" do
          described_class.new [formula_1, formula_2, formula_3]
          expect(formula_1.atoms_map.keys).to match_array(('a'..'f').to_a)
          expect(formula_2.atoms_map.keys).to match_array(('a'..'f').to_a)
          expect(formula_3.atoms_map.keys).to match_array(('a'..'f').to_a)
        end
      end
    end
  end

  describe '==' do
    subject do
      build(:formula_set, formulas:
            [build(:formula, clauses: 'a v b'),
             build(:formula, clauses: 'a ^ c'),
             build(:formula, clauses: 'd')])
    end

    context 'with an equal formula set' do
      let(:formula_set) { subject.clone }

      it 'returns true' do
        expect(subject == formula_set).to be true
      end
    end

    context 'with a formula set with different ordering' do
      let(:formula_set) do
        build(:formula_set, formulas:
              [build(:formula, clauses: 'a ^ c'),
               build(:formula, clauses: 'd'),
               build(:formula, clauses: 'a v b')])
      end

      it 'returns true' do
        expect(subject == formula_set).to be true
      end
    end

    context 'with a different formula set' do
      let(:formula_set) do
        build(:formula_set, formulas:
            [build(:formula, clauses: 'a v b'),
             build(:formula, clauses: 'a ^ c'),
             build(:formula, clauses: 'd'),
             build(:formula, clauses: 'd v a')])
      end

      it 'returns false' do
        expect(subject == formula_set).to be false
      end
    end

    context 'with any other object' do
      let(:formula_set) { build(:formula, :single_clause) }

      it 'returns false' do
        expect(subject == formula_set).to be false
      end
    end
  end

  describe 'prime' do
    context 'without any formula' do
      subject { build(:formula_set, :empty) }
      let(:atoms) { ['a'] }

      it 'is expected to not change the set' do
        prime = subject.prime(atoms)
        expect(prime).to be_a described_class
        expect(prime).to be_empty
      end
    end

    context 'single formula' do
      context 'with a formula that contains only 1 type of atom' do
        subject { build(:formula_set, formulas: [build(:formula, :atomic)]) }
        let(:atom) { 'a' }
        let(:prime_atom) { "a'" }
        let(:atoms) { [atom] }

        it 'is expected to change the only atom in the set to the prime atom' do
          prime = subject.prime(atoms)
          single_formula = prime.first
          expect(single_formula).to include(prime_atom)
          expect(single_formula).not_to include(atom)
        end
      end

      context 'with a formula that contains various atoms' do
        subject { build(:formula_set, formulas: [build(:formula, :multiple_atoms)]) }
        let(:atoms) { %w(a b c d f) }
        let(:primes_hash) { Hash.new { |hash, key| hash[key] = "#{key}'" } }

        it 'is expected to change the atoms in the set to their prime versions' do
          original_formula = subject.first
          prime = subject.prime(atoms)
          new_formula = prime.first
          atoms.each do |atom|
            if original_formula.atoms.include? atom
              expect(new_formula).to include(primes_hash[atom])
              expect(new_formula).not_to include(atom)
            else
              expect(new_formula).not_to include(primes_hash[atom])
              expect(new_formula).not_to include(atom)
            end
          end
        end
      end
    end

    context 'multiple formula' do
      context 'with completely different atoms' do
        let!(:formula_1) { build(:formula, clauses: 'a v b v ¬c') }
        let!(:formula_2) { build(:formula, clauses: '¬d v ¬e v f') }
        subject { build(:formula_set, formulas: [formula_1, formula_2]) }

        context 'using an atom that is not in neither of the formulas' do
          it 'does not change the formulas' do
            prime = subject.prime(['x'])
            expect(prime).to match_array([formula_1, formula_2])
          end
        end

        context 'using an atom that is in one of the formulas' do
          let(:atom) { 'a' }
          let(:new_formula_1) { build(:formula, clauses: "a' v b v ¬c") }

          it 'changes one of the formulas' do
            prime = subject.prime([atom])
            expect(prime).to match_array([new_formula_1, formula_2])
          end

          it 'updates the atoms array' do
            prime = subject.prime([atom])
            expect(prime.atoms).to match_array(%w(a' b c d e f))
          end
        end
      end
    end
  end

  describe 'replace' do
    context 'single formula' do
      let!(:formula) { build(:formula, clauses: "a' v b v cd") }
      subject { build(:formula_set, formulas: [formula]) }

      context 'with the atom' do
        let(:expected_atoms) { %w(a' c cd) }

        it 'replaces the atom in the formula' do
          new_set = subject.replace('b', 'c')
          expect(new_set.atoms).to match_array(expected_atoms)
          expect(new_set.first.atoms_map.keys).to match_array(expected_atoms)
        end
      end

      context 'without the atom' do
        it 'returns a identical set' do
          new_set = subject.replace('x', 'c')
          expect(new_set.atoms).to match_array(subject.atoms)
          expect(new_set.first.atoms_map.keys).to match_array(subject.first.atoms)
        end
      end
    end

    context 'multiple formula' do
      let!(:formula_1) { build(:formula, clauses: "a' v b v cd") }
      let!(:formula_2) { build(:formula, clauses: "e v ¬f v ga'") }
      let!(:formula_3) { build(:formula, clauses: "a' v f v cc") }
      subject { build(:formula_set, formulas: [formula_1, formula_2, formula_3]) }
      let(:expected_atoms) { %w(a' b cd e x ga' cc) }

      context 'with the atom' do
        it 'replaces the atom in each formula that have it' do
          new_set = subject.replace('f', 'x')
          expect(new_set.atoms).to match_array(expected_atoms)
          new_set.each { |formula| expect(formula.atoms_map.keys).to match_array(expected_atoms) }
        end
      end

      context 'without the atom' do
        it 'returns a identical set' do
          new_set = subject.replace('x', 'l')
          expect(new_set.atoms).to match_array(subject.atoms)
          new_set.each { |formula| expect(formula.atoms_map.keys).to match_array(subject.atoms) }
        end
      end
    end
  end

  describe 'satisfiable?' do
    context 'with a single formula' do
      let!(:formula) { build(:formula, :multiple_clauses) }
      subject { build(:formula_set, formulas: [formula]) }

      before do
        formula.expects(:satisfiable?).returns true
      end

      it "is expected to call the formula's 'satisfiable?' method" do
        expect(subject.satisfiable?).to be true
      end
    end

    context 'with multiple formula' do
      let!(:formula_1) { build(:formula, clauses: '¬a ^ b') }
      let!(:formula_2) { build(:formula, clauses: '¬b ^ c') }
      let(:clauses_hash) { { a: 1, b: 2, c: 3 }.stringify_keys }
      subject { build(:formula_set, formulas: [formula_1, formula_2]) }
      let(:clauses) do
        formula_1.clauses + formula_2.clauses
      end

      before do
        file = mock('cnffile')
        CnfFile.expects(:new).returns(file)
        file.expects(:add_clause).with(clauses.first.minisat_format(clauses_hash))
        file.expects(:add_clause).with(clauses[1].minisat_format(clauses_hash))
        file.expects(:add_clause).with(clauses[2].minisat_format(clauses_hash))
        file.expects(:add_clause).with(clauses.last.minisat_format(clauses_hash))
        file.expects(:n_vars=).with(3)
        MinisatReasoner.expects(:satisfiable?).with(file).returns(false)
      end

      it 'is expected to create a different cnf file and use that on the reasoner' do
        expect(subject.satisfiable?).to be false
      end
    end
  end

  describe 'add' do
    let(:formula_to_add) { build(:formula, clauses: 'd v e v f ^ ¬h') }

    context 'when the set is empty' do
      subject { build(:formula_set, :empty) }

      it "adds the formula's atoms to the array" do
        expect(subject.add(formula_to_add)).to be_a FormulaSet
        expect(subject.atoms).to match_array(%w(d e f h))
      end
    end

    context 'when the set is not empty' do
      subject { build(:formula_set, formulas: [build(:formula, clauses: 'a v b ^ c v h')]) }
      let(:atoms_array) { %w(a b c d e f h) }

      it "adds the formula's atoms to the array" do
        expect(subject.add(formula_to_add)).to be_a FormulaSet
        expect(subject.atoms).to match_array(atoms_array)
      end
    end
  end

  describe 'single_formula_format' do
    let(:formula) { build(:formula, clauses: 'a v b') }

    context 'when the set has only one formula' do
      subject { build(:formula_set, formulas: [formula]) }

      it 'returns its only formula' do
        expect(subject.single_formula_format).to eq(formula)
      end
    end

    context 'when the set has multiple formulas' do
      let(:formula_2) { build(:formula, clauses: 'c v d ^ a') }
      let(:formula_3) { build(:formula, clauses: '¬a v d ^ ¬ v a') }
      subject { build(:formula_set, formulas: [formula, formula_2, formula_3]) }

      it 'returns its formulas as conjunct clauses' do
        conjunct = subject.single_formula_format.string_form
        expect(conjunct).to include(formula.string_form)
        expect(conjunct).to include(formula_2.string_form)
        expect(conjunct).to include(formula_3.string_form)
      end
    end
  end

  describe 'merge' do
    let(:formula) { build(:formula, clauses: 'c v d ^ a') }
    let(:formula_2) { build(:formula, clauses: 'b ^ c v f') }
    let(:formula_set) { build(:formula_set, formulas: [formula, formula_2]) }
    subject { build(:formula_set, formulas: [formula]) }

    it 'updates the atoms hash after merging' do
      subject.merge(formula_set)
      expect(subject.atoms).to match_array(%w(a b c d f))
      expect(subject).to include(formula)
      expect(subject).to include(formula_2)
    end
  end
end
