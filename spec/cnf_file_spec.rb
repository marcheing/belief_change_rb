RSpec.describe CnfFile do
  describe 'initialize' do
    context 'bare new file' do
      subject { FactoryGirl.build(:cnf_file) }

      it 'has a commentary with the file name in its header' do
        expect(subject.header).to eq("c #{subject.filename}")
      end

      it 'does not have content' do
        expect(subject.content).to be_empty
      end

      it 'does not have clauses' do
        expect(subject.n_clauses).to eq 0
      end
    end

    context 'filename without extension' do
      subject { build(:cnf_file, :without_extension) }

      it 'is expected to add a .cnf extension to the filename' do
        expect(subject.filename).to end_with('.cnf')
      end
    end
  end

  describe 'add_clause' do
    context 'valid simple clause' do
      subject { build(:cnf_file) }
      let(:clause) { build(:clause, :single_literal) }
      let(:atom_hash) { { a: 1 }.stringify_keys }

      before :each do
        subject.add_clause(clause.minisat_format(atom_hash))
      end

      it 'is expected to add the clause to the content following the rules' do
        # for an overview of the .cnf file rules, check the following address:
        # http://people.sc.fsu.edu/~jburkardt/data/cnf/cnf.html
        expect(subject.content).to match(/1 0/)
      end

      it 'is expected to increment the clause counter' do
        expect(subject.n_clauses).to eq 1
      end
    end
  end

  describe 'write' do
    context 'valid cnf file configuration' do
      subject { build(:cnf_file) }
      let(:clause) { build(:clause, :single_literal) }
      let(:clause_hash) { { 'a' => 1 } }

      before :each do
        subject.add_clause clause.minisat_format(clause_hash)
        subject.n_vars = 1
        file = mock('file')
        File.expects(:open).with(subject.filename, 'w').yields(file)
        file.expects(:puts).with(subject.header)
        file.expects(:puts).with('p cnf 1 1')
        file.expects(:puts).with('1 0')
      end

      it 'creates a file with the current content' do
        subject.write
      end
    end
  end

  describe 'n_vars=' do
    context 'setting n_vars to a smaller number' do
      subject { build(:cnf_file) }

      before do
        subject.n_vars = 2
      end

      it 'raises an ArgumentError Exception' do
        expect { subject.n_vars = 1 }.to raise_error(ArgumentError)
      end
    end
  end

  describe 'clean' do
    subject { FactoryGirl.build(:cnf_file) }

    context 'when the file has been written' do
      before do
        File.expects(:exist?).with(subject.filename).returns(true)
        File.expects(:delete).with(subject.filename)
      end

      it 'deletes the file' do
        subject.clean
      end
    end

    context "when the file hasn't been written" do
      before do
        File.expects(:exist?).with(subject.filename).returns(false)
      end

      it 'raises a RuntimeError' do
        expect { subject.clean }.to raise_error(RuntimeError)
      end
    end
  end
end
