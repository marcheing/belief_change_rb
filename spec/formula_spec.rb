RSpec.describe Formula do
  describe 'initialize' do
    context 'a single clause' do
      subject { build(:formula, :single_clause) }

      it 'is expected to create a formula with one clause' do
        expect(subject.clauses.first).to be_a Clause
        expect(subject.clauses.size).to eq(1)
      end

      context 'with atoms larger than one character' do
        subject { build(:formula, clauses: "a' v ac v ¬dc") }

        it 'is expected to adequately create the atoms' do
          expect(subject.atoms).to match_array(%w(a' ac dc))
        end
      end
    end

    context 'multiple clauses' do
      subject { build(:formula, :multiple_clauses) }

      it 'is expected to create a formula with multiple clauses' do
        expect(subject.clauses.size).to be > 1
        subject.clauses.each { |clause| expect(clause).to be_a Clause }
      end
    end

    context 'with a tautology' do
      context 'and only the tautology' do
        subject { build(:formula, clauses: '⊤') }

        before do
          FormulaParser.expects(:generate_atom).returns('a')
        end

        it 'is expected to create a clause with the tautology and an atom to map it' do
          expect(subject.string_form).to match(/^a OR -a$/)
          expect(subject.clauses.size).to be 1
          expect(subject.atoms.size).to be 1
        end
      end

      context 'and other atoms' do
        subject { build(:formula, clauses: 'a v ⊤') }

        it 'is expected to remove the unnecessary tautology' do
          expect(subject.string_form).to match(/^a$/)
          expect(subject.clauses.size).to be 1
          expect(subject.atoms.size).to be 1
        end
      end
    end

    context 'a contradiction' do
      let(:generated_atom) { 'b' }
      before :each do
        FormulaParser.expects(:generate_atom).returns(generated_atom)
      end

      context 'and only the contradiction' do
        subject { build(:formula, clauses: '⊥') }

        it 'is expected to create a clause with the contradiction and an atom to map it' do
          expect(subject.string_form).to match(/^#{generated_atom} AND -#{generated_atom}$/)
          expect(subject.clauses.size).to be 2
          expect(subject.atoms.size).to be 1
        end
      end

      context 'and other atoms' do
        subject { build(:formula, clauses: 'a ^ ⊥') }

        it 'is expected to create a clause with the contradiction and an atom to map it' do
          expect(subject.string_form).to match(/^a AND #{generated_atom} AND -#{generated_atom}$/)
          expect(subject.clauses.size).to be 3
          expect(subject.atoms.size).to be 2
        end
      end
    end
  end

  describe 'satisfiable?' do
    context 'with a satisfiable formula' do
      subject { FactoryGirl.build(:formula, :satisfiable) }
      before do
        MinisatReasoner.expects(:satisfiable?).returns true
      end

      it 'is expected to return true' do
        expect(subject.satisfiable?).to be true
      end
    end

    context 'with an unsatisfiable formula' do
      subject { FactoryGirl.build(:formula, :unsatisfiable) }

      before do
        MinisatReasoner.expects(:satisfiable?).returns false
      end

      it 'is expected to return false' do
        expect(subject.satisfiable?).to be false
      end
    end
  end

  describe 'atoms' do
    subject { FactoryGirl.build(:formula, clauses: 'a v b ^ ¬b v c') }
    context 'non-modified formula' do
      it 'is expected to return a list with every atom that can be found in the formula' do
        expect(subject.atoms).to eq(%w(a b c))
      end
    end

    context 'after modifying a formula' do
      it 'is expected to replace the pevious atom with the new one' do
        new_formula = subject.prime_var('b')
        expect(new_formula.atoms).to match_array(['a', "b'", 'c'])
      end
    end
  end

  describe 'prime_var' do
    subject { build(:formula, :simple_replacement) }

    context 'with a var that is not in the formula' do
      let(:var) { 'x' }

      it 'returns the same formula' do
        formula_before = subject.string_form
        new_formula = subject.prime_var(var)
        formula_after = new_formula.string_form

        expect(formula_after).to eq(formula_before)
      end
    end

    context 'with a var that is in the formula' do
      let(:var) { 'a' }

      it 'exchanges the var to its prime version' do
        new_formula = subject.prime_var(var)

        expect(new_formula.string_form).to match(/#{var}'/)
        expect(new_formula.string_form).not_to match(/#{var}[^']/)
      end
    end
  end

  describe 'minisat_format' do
    context 'single clause' do
      let(:formula_input) { '¬a v b' }
      let(:expected_output) { ['-1 2'] }

      it 'is expected to return a list with each clause in minisat CNF form' do
        formula = described_class.new(formula_input)
        expect(formula.minisat_format).to eq(expected_output)
      end
    end

    context 'multiple clauses' do
      let(:expected_clauses_to_create) { ['(-a OR b)', '(-c OR d)', '(a OR d)'] }
      let(:formula_input) { '(¬a v b) ^ (¬c v d) ^ (a v d)' }
      let(:expected_output) { ['-1 2', '-3 4', '1 4'] }

      it 'is expected to return a list with each clause in minisat CNF form' do
        formula = described_class.new(formula_input)
        expect(formula.minisat_format).to eq(expected_output)
      end
    end
  end

  describe 'include?' do
    let(:formula) { build(:formula, :atomic) }

    context 'when the formula has a clause with the atom' do
      it 'returns true' do
        expect(formula.include? 'a').to be true
      end
    end

    context 'when the formula does not have a clause with the atom' do
      it 'returns false' do
        expect(formula.include? 'b').to be false
      end
    end
  end

  describe '==' do
    context 'different formulas' do
      let(:formula_1) { build(:formula, clauses: 'a v bc') }
      let(:formula_2) { build(:formula, clauses: 'b v bc') }

      it 'returns false' do
        expect(formula_1 == formula_2).to be false
      end
    end

    context 'identical formulas' do
      let(:formula_1) { build(:formula, clauses: 'a v bc') }
      let(:formula_2) { build(:formula, clauses: 'a v bc') }

      it 'returns true' do
        expect(formula_1 == formula_2).to be true
      end
    end

    context 'equal formulas with different string input' do
      let(:formula_1) { build(:formula, clauses: '(a v bc) ^ ¬d') }
      let(:formula_2) { build(:formula, clauses: 'a v bc ^ ¬d ') }

      it 'returns true' do
        expect(formula_1 == formula_2).to be true
      end
    end

    context 'equal formulas with different atoms hash' do
      let(:formula_1) { build(:formula, clauses: '(a v bc) ^ ¬d') }
      let(:formula_2) { build(:formula, clauses: 'a v bc ^ ¬d ') }

      it 'returns true' do
        formula_1.atoms_map = { a: '5', b: '6', c: '7', d: '8' }
        expect(formula_1 == formula_2).to be true
      end
    end
  end

  describe 'tautology?' do
    context 'simple tautology' do
      subject { build(:formula, clauses: 'a v ¬a') }

      before do
        MinisatReasoner.expects(:tautology?).returns(true)
      end

      it 'returns true' do
        expect(subject.tautology?).to be true
      end
    end

    context 'complex tautology' do
      subject { build(:formula, clauses: 'a v ¬b v ¬a ^ b v ¬a v ¬b') }

      before do
        MinisatReasoner.expects(:tautology?).returns(true)
      end

      it 'returns true' do
        expect(subject.tautology?).to be true
      end
    end

    context 'no tautology' do
      subject { build(:formula, clauses: 'a ^ b v c ^ ⊤') }

      before do
        MinisatReasoner.expects(:tautology?).returns(false)
      end

      it 'returns false' do
        expect(subject.tautology?).to be false
      end
    end
  end

  describe 'contradiction?' do
    context 'simple contradiction' do
      subject { build(:formula, clauses: 'a ^ ¬a') }

      before do
        MinisatReasoner.expects(:satisfiable?).returns(false)
      end

      it 'returns true' do
        expect(subject.contradiction?).to be true
      end
    end

    context 'complex contradiction' do
      subject { build(:formula, clauses: 'a ^ b ^ ¬a ^ ¬b') }

      before do
        MinisatReasoner.expects(:satisfiable?).returns(false)
      end

      it 'returns true' do
        expect(subject.contradiction?).to be true
      end
    end

    context 'no contradiction' do
      subject { build(:formula, clauses: 'a v b v ⊥') }

      before do
        MinisatReasoner.expects(:satisfiable?).returns(true)
      end

      it 'returns true' do
        expect(subject.contradiction?).to be false
      end
    end
  end

  describe 'negated' do
    context 'with one clause' do
      subject { build(:formula, clauses: 'a v b') }

      it 'returns a new formula, with the clause negated' do
        new_formula = subject.negated
        expect(new_formula.string_form).to eq('-a AND -b')
      end
    end

    context 'with multple clauses' do
      subject { build(:formula, clauses: 'a v b ^ b v c') }
      let(:expected_clauses) do
        [build(:clause, input: '-a OR -b'),
         build(:clause, input: '-c OR -a'),
         build(:clause, input: '-b'),
         build(:clause, input: '-b OR -c')]
      end

      it 'returns a new formula, with the clause negated' do
        new_formula = subject.negated
        expect(new_formula.clauses).to match_array(expected_clauses)
      end
    end
  end

  describe 'conjunct' do
    subject { build(:formula, clauses: 'a v b') }

    context 'with an atom' do
      let(:atom) { 'c' }

      it 'returns a new formula by appending the atom in the form of a clause' do
        new_formula = subject.conjunct atom
        expect(new_formula.string_form).to include('AND c')
        expect(new_formula.atoms).to match_array(%w(a b c))
      end
    end

    context 'with a formula' do
      let(:formula) { build(:formula, clauses: 'c v d ^ ¬e') }

      it 'returns a new formula by appending the argument formula' do
        new_formula = subject.conjunct formula
        expect(new_formula.string_form).to include(formula.string_form)
        expect(new_formula.atoms).to match_array(%w(a b c d e))
      end
    end

    context 'with anything else ' do
      let(:argument) { Object.new }

      it 'raises an argument error exception' do
        expect { subject.conjunct argument }.to raise_error(ArgumentError)
      end
    end
  end

  describe 'replace' do
    context 'when changing an atom to its prime form' do
      subject { build(:formula, clauses: 'a v b ^ c v d') }

      it 'updates the clauses as well' do
        result = subject.replace('a', "a'")
        expect(result.clauses.first.literals).to include("a'")
      end
    end
  end
end
