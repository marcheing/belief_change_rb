RSpec.describe FormulaParser do
  describe 'parse' do
    context 'simple formula with one literal' do
      let(:input) { 'a' }

      it "doesn't change the input string" do
        expect(described_class.parse(input)).to eq(input)
      end
    end

    context 'simple formula with negated literal' do
      let(:input) { '¬b' }

      it 'replaces the negation symbol with a minus' do
        expect(described_class.parse(input)).to eq('-b')
      end
    end

    context 'formula with conjunction symbol' do
      let(:input) { 'a ^ b' }

      it "replaces the conjunction symbol with a 'AND'" do
        expect(described_class.parse(input)).to eq('a AND b')
      end
    end

    context 'formula with disjunction symbol' do
      context 'without a conflicting literal' do
        let(:input) { 'a v b' }

        it "replaces the disjunction symbol with a 'OR'" do
          expect(described_class.parse(input)).to eq('a OR b')
        end
      end

      context 'with a conflicting literal' do
        let(:input) { 'a v v v ¬v ^ v' }

        it "replaces only the disjunction symbol with a 'OR'" do
          expect(described_class.parse(input)).to eq('a OR v OR -v AND v')
        end
      end
    end

    context 'formula with unorganized whitespaces' do
      let(:input) { '  c v  a     ^  b   ' }

      it 'reduces the number of whitespaces to 1 between literals and symbols' do
        expect(described_class.parse(input)).to eq('c OR a AND b')
      end
    end

    context 'formula with already parsed content' do
      let(:input) { 'c OR a AND -b' }

      it 'does not change the input' do
        expect(described_class.parse(input)).to eq(input)
      end

      context 'with double negation' do
        let(:input) { 'c OR -a AND --b' }
        it 'correctly manages the input' do
          expect(described_class.parse(input)).to eq('c OR -a AND b')
        end
      end
    end

    context 'formula with multiple character atoms' do
      let(:input) { "a' v b v ¬c" }

      it 'correctly handles the input' do
        expect(described_class.parse(input)).to eq("a' OR b OR -c")
      end
    end

    context 'formula with double negation' do
      let(:input) { '¬a v b v ¬¬c' }

      it 'correctly handles the input' do
        expect(described_class.parse(input)).to eq('-a OR b OR c')
      end
    end
  end

  describe 'parse_minisat_output' do
    let(:minsat_output) { "1 2 -3 4 5 0\n" }
    let(:atoms_map) { { a: 1, b: 2, c: 3, d: 4, e: 5 }.stringify_keys }

    it 'returns a string ready to be used to create a formula' do
      expect(described_class.parse_minisat_output(minsat_output, atoms_map)).to eq('a OR b OR -c OR d OR e')
    end
  end

  describe 'negate_formula_string' do
    let(:formula_string) { 'a OR -b OR c AND -a' }
    let(:atoms_array) { %w(a b c) }

    it 'returns a new string with the atoms negated' do
      expect(described_class.negate_formula_string(formula_string, atoms_array)).to eq('-a OR b OR -c AND a')
    end
  end

  describe 'negate_literal' do
    context 'with a non-negated literal' do
      let(:literal) { 'a' }

      it 'returns a negated literal' do
        expect(described_class.negate_literal literal).to eq('-a')
      end
    end

    context 'with a negated literal' do
      let(:literal) { '-a' }

      it 'returns a non-negated literal' do
        expect(described_class.negate_literal literal).to eq('a')
      end
    end
  end

  describe 'atom_prime_equivalence_string' do
    let(:atom) { 'a' }
    let(:prime) { "a'" }
    let(:expected_string) { "a OR -a' AND -a OR a'" }

    it 'returns a equivalence of the prime of the input atom in CNF form' do
      expect(described_class.atom_prime_equivalence_string(atom)).to eq(expected_string)
    end
  end

  describe 'a_or_b_implies_c' do
    context 'when the literals are a, b and c' do
      it 'returns the CNF form of the expression' do
        expect(described_class.a_or_b_implies_c('a', 'b', 'c')).to eq('¬a v c ^ ¬b v c')
      end
    end
  end
end
