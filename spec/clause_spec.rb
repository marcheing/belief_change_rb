RSpec.describe Clause do
  describe 'initialize' do
    context 'with a single literal' do
      subject { build(:clause, :single_literal) }

      it 'is expected to create an array with the literal' do
        expect(subject.literals.size).to eq(1)
        expect(subject.literals).to include('a')
      end
    end

    context 'with a single negated literal' do
      subject { build(:clause, :negated_literal) }

      it 'is expected to create an array with the literal' do
        expect(subject.literals.size).to eq(1)
        expect(subject.literals).to include('-a')
      end
    end

    context 'with multiple literals' do
      subject { build(:clause, :multiple_literals) }

      it 'is expected to create an array with the literals' do
        expect(subject.literals.size).to be > 1
        expect(subject.literals).to include('-a', '-b', 'c')
      end
    end

    context 'with atoms larger than 1 character' do
      subject { build(:clause, input: 'ac OR -bc') }

      it 'is expected to create an array with the literals' do
        expect(subject.literals.size).to be > 1
        expect(subject.literals).to match_array(['ac', '-bc'])
      end
    end
  end

  describe 'minisat_format' do
    subject { build(:clause, :multiple_literals) }
    let(:input_hash) { { a: 1, b: 2, c: 3, d: 4 }.stringify_keys }

    it 'is expected to replace the literals with the numbers based on the input hash' do
      expect(subject.minisat_format(input_hash)).to eq('-1 -2 3')
    end
  end

  describe 'negate' do
    context 'a clause with one atom' do
      subject { build(:clause, input: 'a') }

      it 'returns its negation in a formula' do
        expect(subject.negate).to be_a Formula
        expect(subject.negate.string_form).to eq('-a')
      end
    end

    context 'a clause with more than one atom' do
      subject { build(:clause, input: 'a OR b OR -c') }

      it 'returns a formula containing the negations' do
        expect(subject.negate).to be_a Formula
        expect(subject.negate.string_form).to eq('-a AND -b AND c')
      end
    end
  end

  describe '==' do
    subject { build(:clause, input: 'a OR -b') }

    context 'with an identical clause' do
      let(:clause) { build(:clause, input: 'a OR -b') }

      it 'returns true' do
        expect(subject == clause).to be true
      end
    end

    context 'with a clause that differs in the order of the literals' do
      let(:clause) { build(:clause, input: '-b OR a') }

      it 'returns true' do
        expect(subject == clause).to be true
      end
    end

    context 'with a different clause' do
      let(:clause) { build(:clause, input: '-b OR c') }

      it 'returns false' do
        expect(subject == clause).to be false
      end
    end
  end

  describe 'size' do
    subject { build(:clause, input: '-c OR d OR a') }
    it 'returns the number of atoms of the clause' do
      expect(subject.size).to eq(subject.literals.size)
    end
  end
end
