RSpec.describe ContractionScenario do
  describe 'initialize' do
    let(:number_of_clauses) { 3 }
    before do
      FormulaSet.any_instance.stubs(:entails?).returns(true)
    end
    context 'Small number of variables' do
      let(:number_of_variables) { 20 }

      context 'one formula' do
        let(:number_of_formulas) { 1 }
        let(:clause_max_size) { 4 }

        context 'when the clause_max_size is equal to the clause_min_size' do
          let(:clause_min_size) { 4 }
          let(:parameters_hash) do
            { number_of_formulas: number_of_formulas, number_of_variables: number_of_variables,
              number_of_clauses: number_of_clauses, clause_min_size: clause_min_size, clause_max_size: clause_max_size }
          end
          subject { build(:contraction_scenario, parameters_hash) }

          it 'creates clauses with the same size' do
            expect(subject.formula_set.size).to eq number_of_formulas
            subject.formula_set.each do |formula|
              formula.clauses.each { |clause| expect(clause.size).to eq clause_min_size }
            end
            subject.formula.clauses.each { |clause| expect(clause.size).to eq clause_min_size }
          end
        end

        context 'when the max_size is greater than the min_size' do
          let(:clause_min_size) { 2 }
          let(:parameters_hash) do
            { number_of_formulas: number_of_formulas, number_of_variables: number_of_variables,
              number_of_clauses: number_of_clauses, clause_min_size: clause_min_size, clause_max_size: clause_max_size }
          end
          subject { build(:contraction_scenario, parameters_hash) }

          it 'creates clauses with a random size inside a range of clause_min_size - clause_max_size' do
            expect(subject.formula_set.size).to eq number_of_formulas
            subject.formula_set.each do |formula|
              formula.clauses.each do |clause|
                expect(clause.size).to be_between(clause_min_size, clause_max_size).inclusive
              end
            end
            subject.formula.clauses.each do |clause|
              expect(clause.size).to be_between(clause_min_size, clause_max_size).inclusive
            end
          end
        end

        context 'when the max_size is smaller than the clause_min_size' do
          let(:clause_min_size) { 5 }

          it 'is expected to throw an ArgumentError' do
            expect do
              ContractionScenario.new(number_of_formulas, number_of_variables,
                                      number_of_clauses, clause_min_size, clause_max_size)
            end.to raise_error(ArgumentError)
          end
        end
      end

      context 'any number of formulas' do
        let(:number_of_formulas) { (30..40).to_a.sample }
        let(:number_of_variables) { 50 }
        let(:clause_min_size) { 30 }
        let(:clause_max_size) { 40 }
        let(:parameters_hash) do
          { number_of_formulas: number_of_formulas, number_of_variables: number_of_variables,
            number_of_clauses: number_of_clauses, clause_min_size: clause_min_size, clause_max_size: clause_max_size }
        end
        subject { build(:contraction_scenario, parameters_hash) }

        it 'is expected to create this exact number of formulas for the formula_set' do
          expect(subject.formula_set.size).to eq number_of_formulas
        end
      end
    end

    context 'overflowing number of variables' do
      let(:number_of_variables) { 40 }
      let(:clause_max_size) { 40 }
      subject do
        build(:contraction_scenario, number_of_variables: number_of_variables,
                                     clause_max_size: clause_max_size)
      end

      it 'is expected to create atom strings accordingly' do
        expect(subject.atom_pool.size).to eq(number_of_variables)
        expect(subject.atom_pool.count { |x| x.size > 1 }).to eq number_of_variables - 26
      end
    end
  end
end
