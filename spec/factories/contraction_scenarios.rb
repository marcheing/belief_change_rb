FactoryGirl.define do
  factory :contraction_scenario do
    number_of_formulas { 20 }
    number_of_variables { 20 }
    number_of_clauses { 2 }
    clause_max_size { 10 }
    clause_min_size { 1 }

    initialize_with do
      ContractionScenario.new(number_of_formulas,
                              number_of_variables,
                              number_of_clauses,
                              clause_min_size,
                              clause_max_size)
    end
  end
end
