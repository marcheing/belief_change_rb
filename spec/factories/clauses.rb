FactoryGirl.define do
  factory :clause do
    trait :single_literal do
      input { 'a' }
    end

    trait :negated_literal do
      input { '-a' }
    end

    trait :multiple_literals do
      input { '-a OR -b OR c' }
    end

    initialize_with { Clause.new input }
  end
end
