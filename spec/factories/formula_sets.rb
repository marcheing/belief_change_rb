FactoryGirl.define do
  factory :formula_set do
    formulas { [FactoryGirl.build(:formula, :multiple_clauses)] }

    trait :empty do
      formulas []
    end

    initialize_with { new(formulas) }
  end
end
