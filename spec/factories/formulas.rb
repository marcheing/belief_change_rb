FactoryGirl.define do
  factory :formula do
    trait :single_clause do
      clauses { '¬a v b' }
    end

    trait :multiple_clauses do
      clauses { '(¬a v b) ^ (c v ¬d)' }
    end

    trait :simple_replacement do
      clauses { '(¬a v b) ^ (b v ¬a)' }
    end

    trait :unsatisfiable do
      clauses { '¬a ^ a' }
    end

    trait :atomic do
      clauses { 'a' }
    end

    trait :multiple_atoms do
      clauses { 'a v ¬b v c v ¬d v e v a v ¬b' }
    end

    trait :satisfiable do
      multiple_clauses
    end

    initialize_with { new(clauses) }
  end
end
