FactoryGirl.define do
  factory :cnf_file do
    filename 'test.cnf'

    trait :without_extension do
      filename 'test_file'
    end

    initialize_with { CnfFile.new filename }
  end
end
