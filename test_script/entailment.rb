require File.expand_path('../../config/environment', __FILE__)

def execute_contraction(scenario, contraction_name)
  result = scenario.formula_set.send(contraction_name, scenario.formula)
  return result
rescue RuntimeError => e
  raise e
end

def p_bool(bool)
  bool ? 'True' : 'False'
end

def print_entailment(result_name, result)
  entail_1 = @kernel.entails_all? result
  entail_2 = @smallest.entails_all? result
  entail_3 = @single.entails_all? result
  entail_4 = @full_meet.entails_all? result
  entail_5 = @maxichoice.entails_all? result
  entail_6 = @belief_change.entails_all? result
  printf("%s,%s,%s,%s,%s,%s,%s\n", result_name, p_bool(entail_1), p_bool(entail_2), p_bool(entail_3), p_bool(entail_4), p_bool(entail_5), p_bool(entail_6))
end

def print_header
  printf('Entails?,')
  contractions_string = 'Kernel ~ Full incision,Kernel ~ Smallest incision,' \
    'Partial Meet ~ Single Selection,Partial Meet ~ Full Meet,Partial Meet ~ Maxichoice,Belief Change Extension'
  printf("%s\n", contractions_string, contractions_string)
end

clause_size = 3 # Doing testes with 3SAT
var_number = 20
clause_number = 1
number_of_formulas = 15
scenarios = (1..20).map { ContractionScenario.new(number_of_formulas, var_number, clause_number, clause_size, clause_size, false) }
scenarios.each do |scenario|
  print_header
  @kernel = execute_contraction(scenario, 'full_incision_kernel_contraction')
  @smallest = execute_contraction(scenario, 'smallest_incision_kernel_contraction')
  @single = execute_contraction(scenario, 'single_selection_partial_meet_contraction')
  @full_meet = execute_contraction(scenario, 'full_meet_contraction')
  @maxichoice = execute_contraction(scenario, 'maxichoice_partial_meet_contraction')
  @belief_change = execute_contraction(scenario, 'belief_change_contraction')
  @belief_change = FormulaSet.new(@belief_change.string_form.split(' AND ').map { |x| Formula.new x })
  print_entailment('Kernel ~ Full Incision', @kernel)
  print_entailment('Kernel ~ Smallest Incision', @smallest)
  print_entailment('Partial Meet ~ Single Selection', @single)
  print_entailment('Partial Meet ~ Full Meet', @full_meet)
  print_entailment('Partial Meet ~ Maxichoice', @maxichoice)
  print_entailment('Belief Change Extension', @belief_change)
  printf("\n")
end
