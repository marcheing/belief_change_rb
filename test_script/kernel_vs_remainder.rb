require File.expand_path('../../config/environment', __FILE__)
require 'benchmark'

def execute_contraction(scenario, contraction_name)
  time = 0.0
  MinisatReasoner.reset_counter
  GC.start
  GC.disable
  begin
    time += Benchmark.realtime do
      scenario[:set].send(contraction_name, scenario[:formula])
    end
  rescue RuntimeError => e
    raise e
  end
  GC.enable
  [MinisatReasoner.reset_counter, time]
end

printf("Special kernel scenarios\n")
printf("size,kernel,remainder,kernel,remainder\n")
(1..5).each do |size|
  scenario = ContractionScenario.special_kernel_scenario(size)
  fail unless scenario[:set].entails? scenario[:formula]
  kernel = execute_contraction(scenario, 'kernel_contraction')
  remainder = execute_contraction(scenario, 'partial_meet_contraction')
  printf("%d,%d,%d,%f,%f\n", size, kernel.first, remainder.first, kernel.last, remainder.last)
end

printf("\nSpecial remainder scenarios\n")
printf("size,kernel,remainder,kernel,remainder\n")
(1..5).each do |size|
  scenario = ContractionScenario.special_remainder_scenario(size)
  fail unless scenario[:set].entails? scenario[:formula]
  kernel = execute_contraction(scenario, 'kernel_contraction')
  remainder = execute_contraction(scenario, 'partial_meet_contraction')
  printf("%d,%d,%d,%f,%f\n", size, kernel.first, remainder.first, kernel.last, remainder.last)
end
