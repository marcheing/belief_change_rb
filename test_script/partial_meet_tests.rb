require File.expand_path('../../config/environment', __FILE__)
require 'sqlite3'
require 'benchmark'

def get_sentence_set(id, db)
  FormulaSet.new db.execute('SELECT representation FROM sentence_set a1, rel_sentence_set_sentence a2, sentence a3 '\
 "WHERE a1.id = a2.id_sentence_set AND a2.id_sentence = a3.id AND a1.id = #{id};").map do |sentence_representation|
    Formula.new sentence_representation['representation']
  end
end

def get_sentence(id, db)
  Formula.new db.execute("SELECT representation FROM sentence WHERE id = #{id};").first['representation']
end

begin
  db = SQLite3::Database.new 'horn.db'
  db.results_as_hash = true

  db.execute 'SELECT * FROM experiment;' do |experiment|
    result = get_sentence_set(experiment['id_result'], db)
    db.execute "SELECT * FROM contraction_case WHERE id = #{experiment['id_contraction_case']}" do |contraction_case|
      formula_set = get_sentence_set(contraction_case['id_sentence_set'], db)
      formula = get_sentence(contraction_case['id_sentence'], db)
      #Benchmark.realtime do
        contraction_result = formula_set.full_meet_contraction(formula)
      #end > (experiment['time'].to_i / (10**9))
      unless contraction_result == result
        puts "Experiment differ in result: id #{experiment['id']}"
        contraction_result.pretty_print
        result.pretty_print
      end
    end
  end
rescue SQLite3::Exception => e
  puts 'An exception ocurred while reading the database: '
  puts e

ensure
  db.close unless db.nil?
end
