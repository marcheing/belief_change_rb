require File.expand_path('../../config/environment', __FILE__)
require 'benchmark'

def execute_contraction(scenarios, contraction_name)
  time = 0.0
  MinisatReasoner.reset_counter
  scenarios.each do |scenario|
    GC.start
    GC.disable
    begin
      time += Benchmark.realtime do
        scenario.formula_set.send(contraction_name, scenario.formula)
      end
    rescue RuntimeError => e
      raise e
    end
    GC.enable
  end
  [MinisatReasoner.reset_counter.to_f / scenarios.size.to_f, time / scenarios.size]
end

clause_size = 3 # Doing testes with 3SAT
printf('formulas,variables,clauses,')
contractions_string = 'Kernel ~ Full incision,Kernel ~ Smallest incision,' \
  'Partial Meet ~ Single Selection,Partial Meet ~ Full Meet,Partial Meet ~ Maxichoice,Belief Change Extension'
printf("%s,%s\n", contractions_string, contractions_string)
(20..20).step(5) do |var_number|
  clause_number = var_number / 2
  (5..25).each do |number_of_formulas|
    scenarios = (1..20).map { ContractionScenario.new(number_of_formulas, var_number, clause_number, clause_size, clause_size, false) }
    kernel = execute_contraction(scenarios, 'full_incision_kernel_contraction')
    smallest = execute_contraction(scenarios, 'smallest_incision_kernel_contraction')
    single = execute_contraction(scenarios, 'single_selection_partial_meet_contraction')
    full_meet = execute_contraction(scenarios, 'full_meet_contraction')
    maxichoice = execute_contraction(scenarios, 'maxichoice_partial_meet_contraction')
    belief_change = execute_contraction(scenarios, 'belief_change_contraction')
    printf("%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n", number_of_formulas, var_number, clause_number,
           kernel.first, smallest.first, single.first, full_meet.first, maxichoice.first, belief_change.first,
           kernel.last, smallest.last, single.last, full_meet.last, maxichoice.last, belief_change.last)
  end
end
