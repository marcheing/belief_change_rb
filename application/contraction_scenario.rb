class ContractionScenario
  attr_accessor :formula, :formula_set
  attr_reader :atom_pool

  MAX_ASCII_LETTER = 'z'

  def initialize(number_of_formulas, number_of_variables, number_of_clauses, clause_min_size, clause_max_size,
                 horn = true)
    fail(ArgumentError, 'The clause_max_size should be greater than or equal to the min size') unless \
      clause_max_size >= clause_min_size
    fail(ArgumentError, 'The clause_max_size should be smaller than or equal to the number of variables') unless \
      clause_max_size <= number_of_variables
    formula_function = horn ? :generate_random_horn_formula : :generate_random_formula
    @atom_pool = (1..number_of_variables).map { |i| ContractionScenario.atom_from_int i }
    @formula_size = 1
    @clause_size = (clause_min_size..clause_max_size).to_a
    @formula_set = FormulaSet.new number_of_formulas.times.map { send(formula_function) }
    loop do
      @formula = send formula_function
      break if @formula_set.entails? formula
    end
  end

  def self.special_kernel_scenario(size)
    set = FormulaSet.new
    positive_literal = atom_from_int 1
    (1..size).each do |number|
      literal = atom_from_int(number + 1)
      set += [Formula.new(literal)]
      set += [Formula.new("#{FormulaParser::NEGATION_SYMBOL}#{literal} #{FormulaParser::DISJUNCTION_SYMBOL} #{positive_literal}")]
    end
    { set: set, formula: Formula.new(positive_literal) }
  end

  def self.special_remainder_scenario(size)
    set = FormulaSet.new
    positive_literal = atom_from_int 1
    (1..size).each do |number|
      literal_1 = atom_from_int(number + 1)
      literal_2 = "#{literal_1}'"
      set += [Formula.new(literal_1)]
      set += [Formula.new(literal_2)]
      if number == size
        set += [Formula.new(FormulaParser.a_or_b_implies_c(literal_1, literal_2, positive_literal))]
      else
        next_literal_1 = atom_from_int(number + 2)
        next_literal_2 = "#{next_literal_1}'"
        set += [Formula.new(FormulaParser.a_or_b_implies_c(literal_1, literal_2, next_literal_1))]
        set += [Formula.new(FormulaParser.a_or_b_implies_c(literal_1, literal_2, next_literal_2))]
      end
    end
    { set: set, formula: Formula.new(positive_literal) }
  end

  def self.atom_from_int(int)
    return (int + 96).chr(Encoding::UTF_8) unless int > 26
    new_int = int - 26
    atom_from_int(new_int) + MAX_ASCII_LETTER
  end

  private

  def generate_random_horn_clause
    atoms = @atom_pool.sample(@clause_size.sample)
    atoms.map! { |atom| atom.gsub(/^/, FormulaParser::NEGATION_SYMBOL) }
    atoms.sample.gsub!(FormulaParser::NEGATION_SYMBOL, '') if [true, false].sample
    atoms.join(" #{FormulaParser::DISJUNCTION_SYMBOL} ")
  end

  def generate_random_horn_formula
    clauses = (1..@formula_size).map { generate_random_horn_clause }
    Formula.new clauses.join(" #{FormulaParser::CONJUNCTION_SYMBOL} ")
  end

  def generate_random_cnf_clause
    atoms = @atom_pool.sample(@clause_size.sample)
    atoms.map! { |atom| [true, false].sample ? atom.gsub(/^/, FormulaParser::NEGATION_SYMBOL) : atom }
    atoms.join(" #{FormulaParser::DISJUNCTION_SYMBOL} ")
  end

  def generate_random_formula
    clauses = (1..@formula_size).map { generate_random_cnf_clause }
    Formula.new clauses.join(" #{FormulaParser::CONJUNCTION_SYMBOL} ")
  end
end
