require 'set'

require File.expand_path('../modules/kernel_contractions.rb', __FILE__)
require File.expand_path('../modules/partial_meet_contractions.rb', __FILE__)

# A set of formulas, possibly representing a knowledge base or set
class FormulaSet < Set
  attr_reader :atoms
  include KernelContractions
  include PartialMeetContractions

  # TODO: fix creation for multiple formulas
  def initialize(formulas = [])
    super
    update_atoms_hashes
  end

  def add(formula)
    super
    update_atoms_hashes
    self
  end

  def merge(enum)
    merged = super
    merged.update_atoms_hashes
    merged
  end

  def ==(other)
    return false unless other.is_a? FormulaSet
    all? { |a| other.any? { |b| b == a } } && other.all? { |b| any? { |a| a == b } }
  end

  def prime(atoms)
    atoms.uniq!
    return clone if atoms.empty? || (@atoms - atoms) == @atoms
    FormulaSet.new map do |formula|
      atoms.each do |atom|
        formula = formula.prime_var(atom)
      end
      formula
    end
  end

  def replace(atom_1, atom_2)
    return clone unless @atoms.include? atom_1
    FormulaSet.new map do |formula|
      formula.replace(atom_1, atom_2)
    end
  end

  def satisfiable?
    size == 1 ? first.satisfiable? : MinisatReasoner.satisfiable?(create_cnf_file)
  end

  def contradiction?
    !empty? && !satisfiable?
  end

  def entails?(f)
    !(self + [Formula.new(f.negated.string_form)]).satisfiable?
  end

  def entails_all?(formula_set)
    formula_set.all? { |formula| entails? formula }
  end

  def entails_any?(formula_set)
    formula_set.any? { |formula| entails? formula }
  end

  def belief_change_contraction(formula)
    scenario = BeliefChangeScenario.new
    scenario.k = duplicate
    scenario.r = FormulaSet.new
    scenario.c = FormulaSet.new [formula]
    scenario.belief_change
  end

  def single_formula_format
    Formula.new each.map(&:string_form).join(" #{FormulaParser::REPLACED_CONJUNCTION_SYMBOL} ")
  end

  def pretty_print
    print "{\n"
    each { |f| print "  #{f.string_form}\n" }
    print "}\n"
  end

  def update_atoms_hashes
    each(&:collect_atoms)
    @atoms = flat_map(&:atoms).uniq
    atoms_map = Hash[@atoms.zip(1..@atoms.size)]
    each { |formula| formula.atoms_map = atoms_map }
  end

  def duplicate
    FormulaSet.new map { |f| Formula.new f.string_form }
  end

  private

  def create_cnf_file
    file = CnfFile.new
    file.n_vars = first.atoms_map.length
    flat_map(&:minisat_format).each { |clause| file.add_clause clause }
    file
  end
end
