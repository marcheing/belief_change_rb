# This class is a wrapper for the necessary functions to create a
# dimacs cnf following the suggestions at
# http://www.satlib.org/Benchmarks/SAT/satformat.ps
class CnfFile
  EXTENSION = '.cnf'
  CLAUSE_LINE_END = '0'

  attr_accessor :filename, :n_vars
  attr_reader :header, :content, :n_clauses

  def initialize(name = 'tmp.cnf')
    self.filename = name
    @header = "c #{@filename}"
    @content = ''
    @n_clauses = @n_vars = 0
  end

  def filename=(name)
    if File.extname(name) == (EXTENSION)
      @filename = name
    else
      @filename = name + EXTENSION
    end
  end

  def add_clause(clause)
    @content << "\n" unless @content.empty?
    @content << "#{clause} "
    @content << CLAUSE_LINE_END
    @n_clauses += 1
  end

  def write
    prepare_problem_line
    File.open(@filename, 'w') do |file|
      file.puts(@header)
      file.puts(@problem_line)
      file.puts(@content)
    end
  end

  def clean
    fail("Attempted to remove a file that wasn't created") \
      unless File.exist? @filename
    File.delete(@filename)
  end

  def n_vars=(value)
    fail(ArgumentError, "Invalid assignment of number of variables. There
                            are more variables than #{value} according to the
                            previously added clauses") if value < @n_vars
    @n_vars = value
  end

  private

  def prepare_problem_line
    @problem_line = "p cnf #{n_vars} #{n_clauses}"
  end
end
