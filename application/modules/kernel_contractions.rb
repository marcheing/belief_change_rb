module KernelContractions
  def smallest_incision_kernel_contraction(formula)
    k = smallest_incision(duplicate.kernel(formula))
    new_set = clone
    k.each { |f| new_set.delete_if { |x| x == f } }
    new_set
  end

  def full_incision_kernel_contraction(formula)
    k = full_incision(duplicate.kernel(formula))
    new_set = clone
    k.each { |f| new_set.delete_if { |x| x == f } }
    new_set
  end

  def smallest_incision(k)
    incision = FormulaSet.new
    k.each do |formula_set|
      chosen = nil
      formula_set.each do |formula|
        chosen = choose_better(chosen, formula)
      end
      incision += [chosen]
    end
    incision
  end

  def full_incision(k)
    incision = FormulaSet.new
    k.each do |formula_set|
      formula_set.each do |formula|
        incision += [formula]
      end
    end
    incision
  end

  def choose_better(formula_1, formula_2)
    return formula_2 if formula_1.nil?
    clauses_1 = formula_1.clauses
    clauses_2 = formula_2.clauses
    if clauses_1.size != clauses_2.size
      return clause_1.size < clauses_2.size ? formula_1 : formula_2
    else
      clauses_1.each_index do |clause_index|
        if clauses_1[clause_index].literals.size != clauses_2[clause_index].literals.size
          return clauses_1[clause_index].size < clauses_2[clause_index].size ? formula_1 : formula_2
        end
        clauses_1[clause_index].literals.each_index do |literal_index|
          literal_1 = clauses_1[clause_index].literals[literal_index].gsub(FormulaParser::REPLACED_NEGATION_SYMBOL, '')
          literal_2 = clauses_2[clause_index].literals[literal_index].gsub(FormulaParser::REPLACED_NEGATION_SYMBOL, '')
          literal_value_1 = formula_1.atoms_map[literal_1]
          literal_value_2 = formula_2.atoms_map[literal_2]
          return literal_value_1 < literal_value_2 ? formula_1 : formula_2
        end
      end
    end
    formula_1
  end

  def kernel(formula)
    cut = Set[]
    queue = []
    s = kernel_black_box(formula)
    the_kernel = Set[s]
    s.each { |f| queue.unshift FormulaSet.new [f] }
    until queue.empty?
      hn = queue.pop
      next if cut.any? { |c| hn.include? c }
      new_set = clone - hn
      if new_set.entails? formula
        s = new_set.kernel_black_box formula
        the_kernel += [s]
        s.each { |f| queue.unshift(hn + [f]) }
      else
        cut += [hn]
      end
    end
    the_kernel
  end

  def kernel_black_box(formula)
    new_set = FormulaSet.new
    each do |f|
      new_set += [f]
      break if new_set.entails? formula
    end
    shrunk = new_set
    new_set.each do |f|
      without_formula = shrunk - [f]
      shrunk = without_formula if without_formula.entails? formula
    end
    shrunk
  end

  def package_kernel_black_box(formula_set)
    b_prime = FormulaSet.new
    each do |beta|
      b_prime += [beta]
      break if b_prime.entails_any? formula_set
    end
    b_prime.each do |beta|
      new_set = b_prime - [beta]
      b_prime = new_set if new_set.entails_any? formula_set
    end
    b_prime
  end

  def package_kernel(formula_set)
    queue = []
    s = package_kernel_black_box formula_set
    the_kernel = FormulaSet.new(s)
    s.each { |f| queue.unshift FormulaSet.new [f] }
    until queue.empty?
      hn = queue.pop
      b_prime = clone - [hn]
      next unless b_prime.entails_any?(formula_set)
      s = b_prime.package_kernel_black_box(formula_set)
      the_kernel += s
      s.each { |f| queue.unshift(hn + FormulaSet.new([f])) }
    end
    the_kernel
  end

  def package_kernel_contraction(formula_set)
    dup - package_kernel(formula_set)
  end
end
