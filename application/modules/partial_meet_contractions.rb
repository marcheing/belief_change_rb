module PartialMeetContractions
  def remainder_set(formula)
    set = reiter(duplicate.kernel(formula))
    remainder = Set.new
    set.each do |s|
      copy = duplicate
      s.each { |x| copy.delete_if { |f| f == x } }
      remainder << copy
    end
    remainder
  end

  def expand(formula)
    expanded = FormulaSet.new
    each do |b|
      expanded += [b]
      return expanded unless (self - expanded).entails? formula
    end
    nil
  end

  def shrink(expanded, formula)
    return nil if expanded.nil?
    shrunk = expanded
    expanded.each do |b|
      shrunk -= [b] unless (self - (shrunk - [b])).entails? formula
    end
    shrunk
  end

  def single_selection_partial_meet_contraction(formula)
    single_selection(duplicate.partial_meet(formula))
  end

  def full_meet_contraction(formula)
    full_selection(duplicate.partial_meet(formula))
  end

  def full_selection(remainder_set)
    result = remainder_set.first
    remainder_set.delete result
    remainder_set.each do |set|
      result.delete_if { |x| !set.any? { |z| x == z } }
    end
    result
  end

  def single_selection(remainder_set)
    remainder_set.first
  end

  def maxichoice_partial_meet_contraction(formula)
    maxichoice(duplicate.partial_meet(formula))
  end

  def maxichoice(remainder_set)
    maximum_size = remainder_set.map(&:size).max
    maximum_sets = remainder_set.find_all { |x| x.size == maximum_size }
    result = maximum_sets.first
    maximum_sets.delete_at 0
    maximum_sets.each do |set|
      result.delete_if { |x| !set.any? { |z| x == z } }
    end
    result
  end

  def remainder_black_box(formula, x)
    x_prime = x.clone
    each do |beta|
      new_set = x_prime + [beta]
      x_prime = new_set unless new_set.entails? formula
    end
    x_prime
  end

  def partial_meet(formula)
    queue = []
    s = remainder_black_box(formula, FormulaSet.new)
    remainder = Set[s]
    (clone - s).each { |f| queue.unshift FormulaSet.new [f] }
    until queue.empty?
      hn = queue.pop
      next if hn.entails? formula
      s = remainder_black_box(formula, hn)
      remainder += [s]
      (clone - s).each { |f| queue.unshift(hn + [f]) }
    end
    remainder
  end

  def partial_meet_contraction(formula)
    duplicate.partial_meet(formula).max { |a, b| a.length <=> b.length }
  end

  def reiter(class_of_sets)
    cut = Set.new
    queue = []
    s = class_of_sets.first
    s.each { |c| queue.unshift FormulaSet[c] }
    until queue.empty?
      hn = queue.pop
      next if cut.any? { |x| hn.include? x }
      s = class_of_sets.find { |c| (hn & c).empty? }
      if !s.nil?
        s.each { |x| queue.unshift(hn + [x]) }
      else
        cut += [hn]
      end
    end
    cut
  end
end
