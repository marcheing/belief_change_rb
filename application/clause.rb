# Defining a clause in CNF form
class Clause
  attr_reader :literals

  def initialize(clause_str)
    @literals = clause_str.scan(/#{FormulaParser::REPLACED_NEGATION_SYMBOL}?[a-z][^ )]*/)
  end

  def minisat_format(var_map)
    @literals.map do |literal|
      if literal.start_with? FormulaParser::REPLACED_NEGATION_SYMBOL
        atom = var_map[literal.tr(FormulaParser::REPLACED_NEGATION_SYMBOL, '')]
        "#{FormulaParser::REPLACED_NEGATION_SYMBOL}#{atom}"
      else
        var_map[literal]
      end
    end.join(' ')
  end

  def negate
    Formula.new @literals.map { |l| FormulaParser.negate_literal l }
      .join(" #{FormulaParser::REPLACED_CONJUNCTION_SYMBOL} ")
  end

  def ==(other)
    return false unless other.is_a? Clause
    @literals.sort == other.literals.sort
  end

  def size
    @literals.size
  end
end
