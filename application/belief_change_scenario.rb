# Definition of a belief change scenario according to the formal definition
# seen on Section 4 of "A consistency-based approach for belief change"
class BeliefChangeScenario
  attr_accessor :k, :r, :c
  # k is a knowledge base
  # The formulas in r are contained in the resulting knowledge base
  # The formulas in c aren't contained in the resulting knowledge base
  # For contraction, r is empty and c has size 1
  # For revision, c is empty and r has size 1

  def belief_change
    return Formula.new('⊥') if @k.contradiction? || @r.contradiction?
    in_set = Set.new
    out_set = Set.new
    at = @k.atoms & (@r.atoms + @c.atoms)
    k_prime = k.prime(at)
    at.each do |a|
      equivalence_set = create_equivalence_set(in_set, k_prime, a)
      if equivalence_set.satisfiable? && @c.all? { |phi| !equivalence_set.entails?(phi) }
        in_set += [a]
      else
        out_set += [a]
      end
    end
    in_set.each { |p| k_prime = k_prime.replace("#{p}'", "#{p}") }
    out_set.each { |p| k_prime = k_prime.replace("#{p}'", "#{FormulaParser::REPLACED_NEGATION_SYMBOL}#{p}") }
    @r.empty? ? k_prime.single_formula_format : k_prime.single_formula_format.conjunct(@r.single_formula_format)
  end

  private

  def contraction_check(atoms_set, k)
    combinations = power_set(atoms_set)
    formulas = combinations.map do |atoms_to_negate|
      new_set = atoms_set.map do |atom|
        atoms_to_negate.include?(atom) ? "#{FormulaParser::REPLACED_NEGATION_SYMBOL}#{atom}" : atom
      end
      Formula.new(new_set.join(" #{FormulaParser::REPLACED_CONJUNCTION_SYMBOL} "))
    end
    formulas.find_all { |f| k.conjunct(f).satisfiable? }
  end

  def power_set(set)
    set.inject([[]]) do |acc, you|
      ret = []
      acc.each do |i|
        ret << i
        ret << i + [you]
      end
      ret
    end
  end

  def create_equivalence_set(atoms_set, k_prime, atom)
    new_set = FormulaSet.new
    atoms_set.each do |a|
      new_set.add(Formula.new(FormulaParser.atom_prime_equivalence_string(a)))
    end
    new_set.add(Formula.new(FormulaParser.atom_prime_equivalence_string(atom)))
    k_prime + r + new_set
  end
end
