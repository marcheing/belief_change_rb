require 'yaml'
require 'English'
require 'open3'

# Abstraction to a call to the minisat program: http://www.minisat.se/
class MinisatReasoner
  COMMAND = YAML.load_file('config/constants.yml')['minisat_command']
  SATISFIABLE = 10
  UNSATISFIABLE = 20
  OUTPUT_FILENAME = 'output'
  @reasoner_calls = 0

  def self.satisfiable?(file)
    @reasoner_calls += 1
    file.write
    command = "#{COMMAND} #{file.filename}"
    status = 0
    Open3.popen3(command) do |_stdin, _stdout, stderr, wait_thr|
      error = stderr.read
      unless error.empty?
        file.clean
        p file
        fail("MinisatError: #{error}")
      end
      status = wait_thr.value.exitstatus
    end
    file.clean
    status == SATISFIABLE
  end

  def self.clean_output_file
    fail("Attempted to remove a file that wasn't created") \
      unless File.exist? OUTPUT_FILENAME
    File.delete(OUTPUT_FILENAME)
  end

  # Running in time O(2^n)
  def self.tautology?(file, atoms_map)
    evaluations = 0
    max_evaluations = 2**atoms_map.size
    loop do
      file.write
      `#{COMMAND} #{file.filename} #{OUTPUT_FILENAME}`
      file.clean
      if $CHILD_STATUS.exitstatus == UNSATISFIABLE
        File.delete OUTPUT_FILENAME
        return false
      end
      evaluations += 1
      evaluation = obtain_evaluation_from_minisat_output(atoms_map)
      negated_evaluation = FormulaParser.negate_formula_string(evaluation, atoms_map.keys)
      return true if evaluations == max_evaluations
      file.add_clause Clause.new(negated_evaluation).minisat_format(atoms_map)
    end
  end

  def self.obtain_evaluation_from_minisat_output(atoms_map)
    fail('Output file could not be read') unless File.readable? OUTPUT_FILENAME
    output_file = File.open(OUTPUT_FILENAME, 'r')
    output_file.gets # Ignore first line
    evaluation = FormulaParser.parse_minisat_output(output_file.gets, atoms_map)
    File.delete OUTPUT_FILENAME
    evaluation
  end

  def self.reset_counter
    calls = @reasoner_calls
    @reasoner_calls = 0
    calls
  end
end
