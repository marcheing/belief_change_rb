require 'factory_girl'

# A Fomula Parser will take an input string and returned a prepared string to
# be used by the Formula class
class FormulaParser
  NEGATION_SYMBOL = '¬'
  DISJUNCTION_SYMBOL = 'v'
  CONJUNCTION_SYMBOL = '^'

  REPLACED_NEGATION_SYMBOL = '-'
  REPLACED_DISJUNCTION_SYMBOL = 'OR'
  REPLACED_CONJUNCTION_SYMBOL = 'AND'

  TAUTOLOGY_SYMBOL = '⊤'
  CONTRADICTION_SYMBOL = '⊥'

  ATOM_REGEX = /[a-z][^ )]*/

  def self.parse(input)
    input.gsub!(/  */, ' ')
    input.gsub!(/#{NEGATION_SYMBOL}#{NEGATION_SYMBOL}|#{REPLACED_NEGATION_SYMBOL}#{REPLACED_NEGATION_SYMBOL}/, '')
    input.strip!
    input = handle_tautology(input) if input.include? TAUTOLOGY_SYMBOL
    input = handle_contradiction(input) if input.include? CONTRADICTION_SYMBOL
    input.gsub!(/#{NEGATION_SYMBOL}?#{ATOM_REGEX} #{DISJUNCTION_SYMBOL} (?=#{NEGATION_SYMBOL}?[a-z])/) do |capture|
      capture.gsub(" #{DISJUNCTION_SYMBOL} ", " #{REPLACED_DISJUNCTION_SYMBOL} ")
    end
    input.gsub!(NEGATION_SYMBOL, REPLACED_NEGATION_SYMBOL)
    input.gsub(CONJUNCTION_SYMBOL, REPLACED_CONJUNCTION_SYMBOL)
  end

  def self.handle_tautology(input)
    if input.match(/^#{TAUTOLOGY_SYMBOL}$/)
      generated_atom = generate_atom
      "#{generated_atom} #{REPLACED_DISJUNCTION_SYMBOL} #{REPLACED_NEGATION_SYMBOL}#{generated_atom}"
    else
      input.gsub(/( *#{DISJUNCTION_SYMBOL} *)?#{TAUTOLOGY_SYMBOL}( *#{DISJUNCTION_SYMBOL} *)?/, '')
    end
  end

  def self.handle_contradiction(input)
    generated_atom = generate_atom
    input.gsub("#{CONTRADICTION_SYMBOL}",
               "#{generated_atom} #{REPLACED_CONJUNCTION_SYMBOL} #{REPLACED_NEGATION_SYMBOL}#{generated_atom}")
  end

  def self.generate_atom(blacklist = [])
    # Thanks to https://stackoverflow.com/questions/88311/how-best-to-generate-a-random-string-in-ruby
    loop do
      atom = (0...8).map { (97 + rand(26)).chr }.join
      return atom unless blacklist.include? atom
    end
  end

  def self.parse_minisat_output(minisat_output, atoms_map)
    map = atoms_map.invert.stringify_keys
    map[' '] = " #{REPLACED_DISJUNCTION_SYMBOL} "
    minisat_output.chomp(" 0\n").gsub(/[#{map.keys.join}]/, map)
  end

  def self.negate_formula_string(formula_string, atoms)
    negated = formula_string.clone
    atoms.each do |atom|
      negated.gsub!(/-?#{atom}/, "#{atom}" => "-#{atom}", "-#{atom}" => "#{atom}")
    end
    negated
  end

  def self.negate_literal(literal)
    if literal.start_with? FormulaParser::REPLACED_NEGATION_SYMBOL
      literal[1..-1]
    else
      "#{FormulaParser::REPLACED_NEGATION_SYMBOL}#{literal}"
    end
  end

  def self.create_formula_string_from_clauses(clauses)
    clauses.map! { |c| c.join " #{REPLACED_DISJUNCTION_SYMBOL} " }
    clauses.join " #{REPLACED_CONJUNCTION_SYMBOL} "
  end

  # Returns a string representing the equivalence of an atom and its prime
  # Ex: p <-> p' will be returned as p v ¬p' ^ ¬p v p'
  def self.atom_prime_equivalence_string(atom)
    primed_atom = "#{atom}'"
    "#{atom} #{REPLACED_DISJUNCTION_SYMBOL} #{REPLACED_NEGATION_SYMBOL}#{primed_atom}"\
      " #{REPLACED_CONJUNCTION_SYMBOL} #{REPLACED_NEGATION_SYMBOL}#{atom} #{REPLACED_DISJUNCTION_SYMBOL} #{primed_atom}"
  end

  def self.a_or_b_implies_c(a, b, c)
    a_implies_c = "#{NEGATION_SYMBOL}#{a} #{DISJUNCTION_SYMBOL} #{c}"
    b_implies_c = "#{NEGATION_SYMBOL}#{b} #{DISJUNCTION_SYMBOL} #{c}"
    [a_implies_c, b_implies_c].join(" #{CONJUNCTION_SYMBOL} ")
  end
end
