# Represents a propositional formula or sentence
class Formula
  attr_reader :string_form, :clauses, :atoms
  attr_accessor :atoms_map

  def initialize(clauses_str)
    @string_form = FormulaParser.parse clauses_str
    collect_atoms
    update_atoms_map
  end

  def negated
    return @negated unless @negated.nil?
    @negated = clauses.first.negate if clauses.size == 1
    negated_clauses = []
    negation_recursion(negated_clauses, Array.new(@clauses.size), 0)
    negated_clauses.map!(&:uniq)
    @negated = Formula.new FormulaParser.create_formula_string_from_clauses negated_clauses
  end

  def satisfiable?
    MinisatReasoner.satisfiable? create_cnf_file
  end

  def prime(atoms)
    atoms.uniq!
    formula = Formula.new @string_form
    return formula if atoms.empty? || (@atoms - atoms) == @atoms
    atoms.each do |atom|
      formula = formula.prime_var atom
    end
    formula
  end

  def prime_var(var)
    replace(var, "#{var}'")
  end

  def replace(var, var_2)
    return Formula.new @string_form.gsub(var, var_2) if include? var
    new_formula = clone
    new_formula.collect_atoms
    new_formula.update_atoms_map
    new_formula
  end

  def minisat_format
    @clauses.map { |c| c.minisat_format(@atoms_map) }
  end

  def include?(obj)
    @atoms.include? obj
  end

  def ==(other)
    return false unless other.is_a? Formula
    clauses.flat_map(&:literals) == other.clauses.flat_map(&:literals)
  end

  def collect_atoms
    @negated = nil
    @atoms = @string_form.scan(/[a-z][^ )]*/).uniq
  end

  def tautology?
    MinisatReasoner.tautology?(create_cnf_file, @atoms_map)
  end

  def contradiction?
    !satisfiable?
  end

  def conjunct(input)
    fail ArgumentError, 'You can only conjunct a formula with another formula or atom'\
      unless input.is_a?(Formula) || input.is_a?(String)
    input = Formula.new input if input.is_a? String
    Formula.new("#{@string_form} #{FormulaParser::REPLACED_CONJUNCTION_SYMBOL} #{input.string_form}")
  end

  def update_atoms_map
    @atoms_map = Hash[@atoms.zip(1..@atoms.size)]
    update_clauses
  end

  private

  # TODO: Refactor this method to be less java-esque
  def negation_recursion(negated_clauses, clause_buffer, clause_index)
    if clause_index == clauses.size
      clause = clause_buffer.clone
      negated_clauses << clause
    else
      @clauses[clause_index].literals.each do |literal|
        clause_buffer[clause_index] = FormulaParser.negate_literal literal
        negation_recursion(negated_clauses, clause_buffer, clause_index + 1)
      end
    end
  end

  def new_atom(atom)
    return if @atoms.include? atom
    @atoms << atom
    update_atoms_map
  end

  def create_cnf_file
    file = CnfFile.new
    file.n_vars = @atoms_map.length
    minisat_format.each { |c| file.add_clause c }
    file
  end

  def update_clauses
    @clauses = @string_form.split(" #{FormulaParser::REPLACED_CONJUNCTION_SYMBOL} ").map do |clause_str|
      Clause.new(clause_str)
    end
  end
end
